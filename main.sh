#!/bin/bash
green="\033[0;32m"
red="\033[;31m"
blue="\033[;34m"
purple="\033[;35m"
NC='\033[0m'
yellow="\033[1;33m"


if ping -c 5 artixlinux.org >>/dev/null; then 
  echo -e "${green}You are connected to the internet.${blue}"
elif ping -c 5 archlinux.org >>/dev/null; then
  echo -e "${green}You are connected to the internet."
else
  echo -e "${red}Please connect to the internet and try again."
  exit
fi

ask_for_disk(){
  echo -en "${blue}"
  lsblk
  echo -e "${green}What disk do you want to install to? \n${yellow}WARNING: This will wipe the disk${NC}"
  read disk
  checkdisk
}
checkdisk(){
  echo -en "${yellow}Are you sure you want to install to $disk (Y/n): ${NC}"
  read confirmation
  [[ ! "$confirmation" == [Yy]* ]] && ask_for_disk || echo -e "${green}This will install to $disk"
  disk="${disk#/dev/}"
  disk="/dev/$disk"
}
username(){
  echo -en "${green}What do you want your username to be: ${NC}"
  read user 
  echo -en "${green}What do you want your computers name to be (The root user): ${NC}"
  read host
}
ask_for_password(){
  echo -en "${green}What do you want your password to be: ${NC}"
  read -s pass
  echo -en "\n${green}Confirm your password: ${NC}"
  read -s passconf
  checkpass
}
checkpass(){
  [[ ! "$pass" == "$passconf" ]] && echo -e "${red}Passwords do not match${NC}" && ask_for_password || echo -e "${green}password confirmed"
}

ask_for_disk
username
ask_for_password

pacman -S parted --noconfirm --disable-download-timeout



parted $disk mklabel gpt mkpart efi 1M 400M mkpart btrfs 400M 100% -s 
partprobe
mkfs.fat -F32 $disk"1"
modprobe dm-crypt
modprobe dm-mod
cryptsetup luksFormat -v -s 512 -h sha512 $disk"2" <<<"YES"<<<"$pass"<<<"$pass"
cryptsetup open $disk"2" artix <<<"$pass"
crypt="/dev/mapper/artix"
mkfs.btrfs $crypt
mount $crypt /mnt 
btrfs su cr /mnt/@
btrfs su cr /mnt/@home
btrfs su cr /mnt/@var
btrfs su cr /mnt/@opt
btrfs su cr /mnt/@tmp
btrfs su cr /mnt/@.snapshots
umount /mnt
mount -o noatime,commit=120,compress=zstd,space_cache,subvol=@ $crypt /mnt
mkdir /mnt/{boot,home,var,opt,tmp,.snapshots}
mount -o noatime,commit=120,compress=zstd,space_cache,subvol=@home $crypt /mnt/home
mount -o noatime,commit=120,compress=zstd,space_cache,subvol=@opt $crypt /mnt/opt
mount -o noatime,commit=120,compress=zstd,space_cache,subvol=@tmp $crypt /mnt/tmp
mount -o noatime,commit=120,compress=zstd,space_cache,subvol=@.snapshots $crypt /mnt/.snapshots
mount -o subvol=@var $crypt /mnt/var
mount $disk"1" /mnt/boot
basestrap -C pacman.conf /mnt base base-devel openrc elogind-openrc linux-firmware python-pip grub efibootmgr networkmanager networkmanager-openrc os-prober mtools dosfstools opendoas git --ignore sudo --disable-download-timeout
fstabgen -U /mnt >> /mnt/etc/fstab
cp part2.sh /mnt/part2.sh
echo "export host=$host" >>/mnt/variables.sh
echo "export user=$user" >>/mnt/variables.sh
echo "export pass=$pass" >>/mnt/variables.sh
mv pacman.conf /mnt/etc/pacman.conf
artix-chroot /mnt <<EOF
bash /part2.sh
EOF


