#!/bin/bash
source /variables.sh

pacman-key --recv-key FBA220DFC880C036 --keyserver keyserver.ubuntu.com
pacman-key --lsign-key FBA220DFC880C036
pacman -U 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-keyring.pkg.tar.zst' 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-mirrorlist.pkg.tar.zst'

pacman -S linux-tkg-pds linux-tkg-pds-headers --noconfirm --disable-package-timeout

echo -e "$pass\n$pass" | passwd 
useradd -m $user 
usermod -aG wheel $user
echo -e "$pass\n$pass" | passwd $user

nc=`grep -c ^processor /proc/cpuinfo`
nc=`expr $nc - 1`
sed -i "s/#MAKEFLAGS=\"-j2\"/MAKEFLAGS=\"-j$nc\"/g" /etc/makepkg.conf

pip install tzupdate 
hwclock --systohc

sed -i "s/GRUB_CMDLINE_LINUX=\"\"/GRUB_CMDLINE_LINUX=\"\/$disk"2"\:arch\"/g" /etc/default/grub
sed -i 's/HOOKS=(base udev autodetect modconf block filesystems keyboard fsck)/HOOKS=(base udev autodetect modconf block encrypt filesystems keyboard fsck)/g' /etc/mkinitcpio.conf
sed -i 's/MODULES=()/MODULES=(btrfs)/g' /etc/mkinitcpio.conf

mkinitcpio -p linux-tkg-pds
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id = Artix
grub-mkconfig -o /boot/grub/grub.cfg
echo "permit persist keepenv :wheel" >> /etc/doas.conf

